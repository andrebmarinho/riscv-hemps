onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /test_bench/HeMPS/clock
add wave -noupdate  -group {PE0x0} -group {repositoriy} /test_bench/HeMPS/repo_address
add wave -noupdate  -group {PE0x0} -group {repositoriy} /test_bench/HeMPS/repo_data
add wave -noupdate  -group {PE0x0} -group {repositoriy} /test_bench/HeMPS/ack_app
add wave -noupdate  -group {PE0x0} -group {repositoriy} /test_bench/HeMPS/req_app
add wave -noupdate  -group {PE0x0} -group CPU_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//current_page
add wave -noupdate  -group {PE0x0} -group CPU_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//irq_mask_reg
add wave -noupdate  -group {PE0x0} -group CPU_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//irq_status
add wave -noupdate  -group {PE0x0} -group CPU_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//irq
add wave -noupdate  -group {PE0x0} -group CPU_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//cpu_mem_address_reg
add wave -noupdate  -group {PE0x0} -group CPU_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//cpu_mem_data_write_reg
add wave -noupdate  -group {PE0x0} -group CPU_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//cpu_mem_data_read
add wave -noupdate  -group {PE0x0} -group CPU_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//cpu_mem_write_byte_enable
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/operation
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group send_0_PS -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/tx
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group send_0_PS -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/data_out
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group send_0_PS -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/credit_i
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group receive_0_PS -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/rx
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group receive_0_PS -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/data_in
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group receive_0_PS -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/credit_o
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group config_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/set_address
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group config_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/set_address_2
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group config_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/set_size
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group config_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/set_size_2
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group config_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/set_op
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group config_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/start
add wave -noupdate  -group {PE0x0} -group {DMNI 0} -group config_0 -radix hexadecimal /test_bench/HeMPS/proc(0)/PE//dmni/config_data
add wave -noupdate  -group {PE0x0} -group {router 0} -divider receive
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_EAST 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/rx(0)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_EAST 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/data_in(0)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_EAST 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/credit_o(0)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_WEST 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/rx(1)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_WEST 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/data_in(1)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_WEST 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/credit_o(1)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_NORTH 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/rx(2)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_NORTH 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/data_in(2)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_NORTH 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/credit_o(2)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_SOUTH 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/rx(3)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_SOUTH 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/data_in(3)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_SOUTH 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/credit_o(3)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_LOCAL 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/rx(4)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_LOCAL 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/data_in(4)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {input_LOCAL 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/credit_o(4)
add wave -noupdate  -group {PE0x0} -group {router 0} -divider send
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_EAST 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/tx(0)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_EAST 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/data_out(0)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_EAST 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/credit_i(0)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_WEST 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/tx(1)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_WEST 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/data_out(1)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_WEST 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/credit_i(1)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_NORTH 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/tx(2)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_NORTH 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/data_out(2)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_NORTH 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/credit_i(2)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_SOUTH 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/tx(3)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_SOUTH 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/data_out(3)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_SOUTH 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/credit_i(3)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_LOCAL 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/tx(4)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_LOCAL 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/data_out(4)
add wave -noupdate  -group {PE0x0} -group {router 0} -group {output_LOCAL 0} -radix hexadecimal /test_bench/HeMPS/proc(0)/PE/router/credit_i(4)
add wave -noupdate  -group {PE0x1} -group CPU_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//current_page
add wave -noupdate  -group {PE0x1} -group CPU_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//irq_mask_reg
add wave -noupdate  -group {PE0x1} -group CPU_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//irq_status
add wave -noupdate  -group {PE0x1} -group CPU_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//irq
add wave -noupdate  -group {PE0x1} -group CPU_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//cpu_mem_address_reg
add wave -noupdate  -group {PE0x1} -group CPU_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//cpu_mem_data_write_reg
add wave -noupdate  -group {PE0x1} -group CPU_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//cpu_mem_data_read
add wave -noupdate  -group {PE0x1} -group CPU_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//cpu_mem_write_byte_enable
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/operation
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group send_2_PS -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/tx
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group send_2_PS -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/data_out
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group send_2_PS -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/credit_i
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group receive_2_PS -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/rx
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group receive_2_PS -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/data_in
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group receive_2_PS -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/credit_o
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group config_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/set_address
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group config_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/set_address_2
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group config_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/set_size
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group config_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/set_size_2
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group config_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/set_op
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group config_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/start
add wave -noupdate  -group {PE0x1} -group {DMNI 2} -group config_2 -radix hexadecimal /test_bench/HeMPS/proc(2)/PE//dmni/config_data
add wave -noupdate  -group {PE0x1} -group {router 2} -divider receive
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_EAST 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/rx(0)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_EAST 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/data_in(0)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_EAST 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/credit_o(0)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_WEST 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/rx(1)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_WEST 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/data_in(1)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_WEST 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/credit_o(1)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_NORTH 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/rx(2)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_NORTH 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/data_in(2)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_NORTH 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/credit_o(2)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_SOUTH 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/rx(3)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_SOUTH 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/data_in(3)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_SOUTH 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/credit_o(3)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_LOCAL 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/rx(4)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_LOCAL 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/data_in(4)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {input_LOCAL 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/credit_o(4)
add wave -noupdate  -group {PE0x1} -group {router 2} -divider send
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_EAST 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/tx(0)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_EAST 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/data_out(0)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_EAST 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/credit_i(0)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_WEST 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/tx(1)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_WEST 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/data_out(1)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_WEST 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/credit_i(1)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_NORTH 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/tx(2)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_NORTH 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/data_out(2)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_NORTH 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/credit_i(2)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_SOUTH 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/tx(3)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_SOUTH 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/data_out(3)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_SOUTH 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/credit_i(3)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_LOCAL 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/tx(4)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_LOCAL 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/data_out(4)
add wave -noupdate  -group {PE0x1} -group {router 2} -group {output_LOCAL 2} -radix hexadecimal /test_bench/HeMPS/proc(2)/PE/router/credit_i(4)
add wave -noupdate  -group {PE1x0} -group CPU_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//current_page
add wave -noupdate  -group {PE1x0} -group CPU_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//irq_mask_reg
add wave -noupdate  -group {PE1x0} -group CPU_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//irq_status
add wave -noupdate  -group {PE1x0} -group CPU_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//irq
add wave -noupdate  -group {PE1x0} -group CPU_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//cpu_mem_address_reg
add wave -noupdate  -group {PE1x0} -group CPU_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//cpu_mem_data_write_reg
add wave -noupdate  -group {PE1x0} -group CPU_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//cpu_mem_data_read
add wave -noupdate  -group {PE1x0} -group CPU_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//cpu_mem_write_byte_enable
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/operation
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group send_1_PS -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/tx
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group send_1_PS -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/data_out
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group send_1_PS -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/credit_i
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group receive_1_PS -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/rx
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group receive_1_PS -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/data_in
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group receive_1_PS -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/credit_o
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group config_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/set_address
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group config_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/set_address_2
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group config_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/set_size
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group config_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/set_size_2
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group config_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/set_op
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group config_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/start
add wave -noupdate  -group {PE1x0} -group {DMNI 1} -group config_1 -radix hexadecimal /test_bench/HeMPS/proc(1)/PE//dmni/config_data
add wave -noupdate  -group {PE1x0} -group {router 1} -divider receive
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_EAST 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/rx(0)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_EAST 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/data_in(0)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_EAST 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/credit_o(0)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_WEST 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/rx(1)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_WEST 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/data_in(1)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_WEST 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/credit_o(1)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_NORTH 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/rx(2)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_NORTH 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/data_in(2)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_NORTH 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/credit_o(2)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_SOUTH 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/rx(3)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_SOUTH 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/data_in(3)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_SOUTH 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/credit_o(3)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_LOCAL 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/rx(4)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_LOCAL 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/data_in(4)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {input_LOCAL 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/credit_o(4)
add wave -noupdate  -group {PE1x0} -group {router 1} -divider send
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_EAST 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/tx(0)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_EAST 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/data_out(0)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_EAST 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/credit_i(0)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_WEST 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/tx(1)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_WEST 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/data_out(1)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_WEST 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/credit_i(1)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_NORTH 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/tx(2)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_NORTH 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/data_out(2)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_NORTH 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/credit_i(2)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_SOUTH 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/tx(3)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_SOUTH 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/data_out(3)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_SOUTH 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/credit_i(3)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_LOCAL 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/tx(4)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_LOCAL 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/data_out(4)
add wave -noupdate  -group {PE1x0} -group {router 1} -group {output_LOCAL 1} -radix hexadecimal /test_bench/HeMPS/proc(1)/PE/router/credit_i(4)
add wave -noupdate  -group {PE1x1} -group CPU_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//current_page
add wave -noupdate  -group {PE1x1} -group CPU_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//irq_mask_reg
add wave -noupdate  -group {PE1x1} -group CPU_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//irq_status
add wave -noupdate  -group {PE1x1} -group CPU_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//irq
add wave -noupdate  -group {PE1x1} -group CPU_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//cpu_mem_address_reg
add wave -noupdate  -group {PE1x1} -group CPU_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//cpu_mem_data_write_reg
add wave -noupdate  -group {PE1x1} -group CPU_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//cpu_mem_data_read
add wave -noupdate  -group {PE1x1} -group CPU_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//cpu_mem_write_byte_enable
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/operation
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group send_3_PS -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/tx
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group send_3_PS -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/data_out
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group send_3_PS -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/credit_i
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group receive_3_PS -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/rx
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group receive_3_PS -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/data_in
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group receive_3_PS -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/credit_o
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group config_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/set_address
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group config_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/set_address_2
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group config_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/set_size
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group config_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/set_size_2
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group config_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/set_op
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group config_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/start
add wave -noupdate  -group {PE1x1} -group {DMNI 3} -group config_3 -radix hexadecimal /test_bench/HeMPS/proc(3)/PE//dmni/config_data
add wave -noupdate  -group {PE1x1} -group {router 3} -divider receive
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_EAST 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/rx(0)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_EAST 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/data_in(0)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_EAST 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/credit_o(0)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_WEST 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/rx(1)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_WEST 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/data_in(1)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_WEST 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/credit_o(1)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_NORTH 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/rx(2)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_NORTH 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/data_in(2)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_NORTH 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/credit_o(2)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_SOUTH 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/rx(3)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_SOUTH 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/data_in(3)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_SOUTH 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/credit_o(3)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_LOCAL 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/rx(4)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_LOCAL 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/data_in(4)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {input_LOCAL 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/credit_o(4)
add wave -noupdate  -group {PE1x1} -group {router 3} -divider send
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_EAST 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/tx(0)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_EAST 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/data_out(0)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_EAST 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/credit_i(0)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_WEST 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/tx(1)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_WEST 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/data_out(1)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_WEST 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/credit_i(1)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_NORTH 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/tx(2)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_NORTH 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/data_out(2)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_NORTH 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/credit_i(2)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_SOUTH 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/tx(3)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_SOUTH 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/data_out(3)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_SOUTH 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/credit_i(3)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_LOCAL 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/tx(4)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_LOCAL 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/data_out(4)
add wave -noupdate  -group {PE1x1} -group {router 3} -group {output_LOCAL 3} -radix hexadecimal /test_bench/HeMPS/proc(3)/PE/router/credit_i(4)
configure wave -signalnamewidth 1
configure wave -namecolwidth 217
configure wave -timelineunits ns
