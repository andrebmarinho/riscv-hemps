#ifndef _HEMPS_PKG_
#define _HEMPS_PKG_

#define PAGE_SIZE_BYTES           65536
#define MEMORY_SIZE_BYTES      262144
#define TOTAL_REPO_SIZE_BYTES   1048576
#define APP_NUMBER           1
#define N_PE_X              2
#define N_PE_Y              2
#define N_PE                4

const int pe_type[N_PE] = {1, 0, 0, 0};

#endif
