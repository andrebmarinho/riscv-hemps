.section .text
.align 2
.globl entry

entry:
        #.set noreorder
        li    sp,sp_addr  # set up the stack pointer, using a constant defined in the linker script.

        la    t0, end       # on hardware, ECALL doesn't stop the CPU, so define
                        # a handler to catch the ECALL and spin
        csrw  0x305, t0     # set the address of the handler (CSR 0x305 is the trap handler base register)

        call  main          # call the main function
        ecall               # halt the simluation when it returns

end:
        j end               # loop when finished if there is no environment to return to.
        nop

        #.end entry

################################################

   .globl SystemCall
   #.ent SystemCall
SystemCall:
   #.set	noreorder
   
   #syscall #TODO!
   nop
   jr	ra
   nop
   
   #.set reorder
   #.end SystemCall

