library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
--use ieee.numeric_std.all;

entity clarvi_wrapper is
    generic (
        ADDR_WIDTH    : integer := 29;
        MEM_WIDTH     : integer := 14
    );
    port(
        clock               : in  std_logic :='0';
       	reset               : in  std_logic :='0';
        data_address        : out std_logic_vector(ADDR_WIDTH-1 downto 0);
        wbe_data            : out std_logic_vector(3 downto 0);
        data_writedata      : out std_logic_vector(ADDR_WIDTH+2 downto 0);
        internal_readdata   : in std_logic_vector(ADDR_WIDTH+2 downto 0);
        instr_address       : out std_logic_vector(ADDR_WIDTH-1 downto 0);   
        instr_readdata      : in std_logic_vector(ADDR_WIDTH+2 downto 0);
        instr_writedata     : out std_logic_vector(31 downto 0) := x"00000000";
        wbe_instr           : out std_logic_vector(3 downto 0) := "0000";
        interrupt           : in std_logic;
		current_page 		: out std_logic_vector(7 downto 0) -- TODO
	);
end entity clarvi_wrapper;

architecture wrapper of clarvi_wrapper is 	
	constant IMEM_BASE : natural := 0;
	constant IMEM_END  : natural := ADDR_WIDTH+2;

	-- Memorie interfaces:
	signal data_readdata : std_logic_vector(IMEM_END downto IMEM_BASE); -- 32b
	signal external_readdata : std_logic_vector(IMEM_END downto IMEM_BASE); -- 32b
	
	signal data_byteenable : std_logic_vector(3 downto 0);         -- 4b

	signal data_read : std_logic;
	signal data_write : std_logic;
	signal data_waitrequest : std_logic := '0';
	signal data_readdatavalid : std_logic;
	
	--signal instr_address : std_logic_vector(ADDR_WIDTH-1 downto 0); -- 29b
	signal instr_read : std_logic;
	signal instr_waitrequest : std_logic := '0';
	signal instr_readdatavalid : std_logic := '1';
	
	signal read_from_external : std_logic;
	signal external_access : std_logic;
	
	signal avs_a_write : std_logic;
	signal deb_we : std_logic;

	signal main_data_address : std_logic_vector(ADDR_WIDTH-1 downto 0);

begin
    wbe_data <= data_byteenable WHEN avs_a_write = '1' ELSE "0000";
	wbe_instr <= "0000";
	avs_a_write <= data_write and (not (external_access));
	deb_we <= data_write and external_access;

	--it's an external access if any address bit larger than the memory is non-zero
	external_access <= '1' WHEN unsigned(main_data_address(ADDR_WIDTH-1 downto MEM_WIDTH)) > 0 ELSE '0'; 
	read_from_external <= data_read and external_access;
	data_readdata <= external_readdata WHEN read_from_external = '1' ELSE internal_readdata;

	data_address <= main_data_address;

    clarvi: entity work.clarvi_avalon
		generic map(
			INSTR_ADDR_WIDTH => ADDR_WIDTH,
			DATA_ADDR_WIDTH => ADDR_WIDTH
		) port map(
			clock => clock,
			reset => reset,
			avm_main_address => main_data_address,
			avm_main_byteenable => data_byteenable,
			avm_main_read => data_read,
			avm_main_readdata => data_readdata,
			avm_main_write => data_write,
			avm_main_writedata => data_writedata,
			avm_main_waitrequest => data_waitrequest,
			avm_main_readdatavalid => data_readdatavalid,
			avm_instr_address => instr_address,
			avm_instr_read => instr_read,
			avm_instr_readdata => instr_readdata,
			avm_instr_waitrequest => instr_waitrequest,
			avm_instr_readdatavalid => instr_readdatavalid,
			inr_irq => interrupt,
			debug_register28 => open,
			debug_scratch => open,
			debug_pc => open
		);
end architecture;